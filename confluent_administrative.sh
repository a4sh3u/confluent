#!/usr/bin/env bash

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--list )
echo "$RES"

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--delete \
--topic replicated-topic )
echo "$RES"

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--list )
echo "$RES"

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--create \
--topic reassign \
--replica-assignment 101:102,102:101,101:102,102:101,101:102,102:101 )
echo "$RES"

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--describe \
--topic reassign )
echo "$RES"

RES=$( kafka-producer-perf-test \
--topic reassign \
--num-records 2000000 \
--record-size 1000 \
--throughput 1000000000 \
--producer-props bootstrap.servers=broker101:9092,broker102:9092,broker103:9092 )
echo "$RES"

RES=$( timeout 15s kafka-consumer-perf-test \
--broker-list broker101:9092 \
--topic reassign \
--group test-group \
--threads 1 \
--show-detailed-stats \
--reporting-interval 5000 \
--messages 10000000 )
echo "$RES"

RES=$( confluent-rebalancer execute \
--zookeeper zookeeper1:2181 \
--metrics-bootstrap-server broker101:9092,broker102:9092,broker103:9092 \
--throttle 1000000000 \
--verbose \
--force )
echo "$RES"

COUNT=1
while true;
do
  RES=$( confluent-rebalancer status \
  --zookeeper zookeeper1:2181 )
  echo "$RES"

  if [[ "$RES" =~ "is still in progress" ]]; then
    echo "==> Sleeping 10 seconds before checking rebalancer status again"
    sleep 10
    COUNT=$((COUNT + 1))
    if [[ $COUNT -gt 360 ]]; then
      echo "ERROR: Reassignment not finished after 1 hour"
      exit 1
    fi
  else
    break
  fi
done

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--describe \
--topic reassign )
echo "$RES"

ssh broker101 exec "sudo service kafka-server stop"

echo "Sleeping 20 seconds for Kafka server 101 to stop"
sleep 20

ssh broker101 exec "sudo rm -rf /var/lib/kafka/*"

ssh broker101 exec "sudo service kafka-server start"
echo "Sleeping 20 seconds for Kafka servers to start"
sleep 20

echo "SUCCESS!"

exit 0

