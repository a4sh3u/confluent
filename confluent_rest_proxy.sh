#!/usr/bin/env bash

nohup connect-standalone /etc/kafka/connect-standalone.properties /etc/kafka/connect-file-source.properties /etc/kafka/connect-file-sink.properties > /dev/null 2>&1 &

echo "Waiting for connect to start.  Sleeping 30 seconds"
sleep 30

RES=$( curl http://kafkarest1:8083/connectors )
echo "$RES"

curl http://kafkarest1:8083/connectors/local-file-sink/config > local-file-sink.config.json

cd
sed -i "s/test.sink.txt/test-new.sink.txt/" local-file-sink.config.json

RES=$( curl -X PUT http://kafkarest1:8083/connectors/local-file-sink/config \
-d @local-file-sink.config.json \
--header "Content-Type: application/json" )
echo "$RES"

echo "foobarbaz" >> test.txt
sleep 10

cat test.sink.txt
cat test-new.sink.txt

sudo kill $( ps aux | grep connect-standalone | grep java | awk '{print $2}' )
sleep 5

echo "SUCCESS!"

exit 0
