#!/usr/bin/env bash

cd

TOPIC_NO_SSL="no-ssl-topic"
TOPIC_YES_SSL="ssl-topic"

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--describe \
--topic $TOPIC_NO_SSL )
if [[ "$RES" == "" ]]; then
  RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--create \
--topic $TOPIC_NO_SSL \
--partitions 1 \
--replication-factor 3 )
  echo "$RES"
fi

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--describe \
--topic $TOPIC_YES_SSL )
if [[ "$RES" == "" ]]; then
  RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--create \
--topic $TOPIC_YES_SSL \
--partitions 1 \
--replication-factor 3 )
  echo "$RES"
fi

sleep 5

echo "=> Running kafka-producer-perf-test on topic $TOPIC_NO_SSL without SSL"
RES=$( kafka-producer-perf-test \
--topic $TOPIC_NO_SSL \
--num-records 400000 \
--record-size 1000 \
--throughput 1000000 \
--producer-props bootstrap.servers=broker101:9092,broker102:9092,broker103:9092 \
--producer.config /usr/local/etc/producer.properties )
echo "$RES"

echo "=> Running kafka-producer-perf-test on topic $TOPIC_YES_SSL with SSL"
RES=$( kafka-producer-perf-test \
--producer.config client.properties \
--topic $TOPIC_YES_SSL \
--num-records 400000 \
--record-size 1000 \
--throughput 1000000 \
--producer-props bootstrap.servers=broker101:9093,broker102:9093,broker103:9093 \
--producer.config /usr/local/etc/producer_ssl.properties )
echo "$RES"

exit 0
