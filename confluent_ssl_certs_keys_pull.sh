#!/usr/bin/env bash

BACKUPPATH="/home/training/backup"

HOSTNAME=$( hostname -a )
scp training@zookeeper1:ssl/certs/kafka.$HOSTNAME.truststore.jks /home/training/ssl/certs/.
ssh training@zookeeper1 exec "mv ssl/certs/kafka.$HOSTNAME.truststore.jks $BACKUPPATH/."
scp training@zookeeper1:ssl/keys/kafka.$HOSTNAME.keystore.jks /home/training/ssl/keys/.
ssh training@zookeeper1 exec "mv ssl/keys/kafka.$HOSTNAME.keystore.jks $BACKUPPATH/."
chown training:training /home/training/ssl -R

exit 0
