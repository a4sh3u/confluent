#!/usr/bin/env bash

# Produce Shakespeare works to Topic `shakespeare_topic`
sed -i '/<target>/a <excludes><exclude>partial/*.java</exclude></excludes>' /home/training/developer/exercise_code/kafka_avro/pom.xml
mvn compile -f /home/training/developer/exercise_code/kafka_avro/pom.xml 
mvn exec:java -Dexec.mainClass=solution.ShakespeareProducer -f /home/training/developer/exercise_code/kafka_avro/pom.xml
sed -i '/<excludes>/d' /home/training/developer/exercise_code/kafka_avro/pom.xml

echo -e "\nSUCCESS!"

exit 0
