#!/usr/bin/env bash

# Install Confluent Open Source
sudo yum -y install confluent-platform-2.11

# Set broker.id
HOSTNAME=$( hostname -a )
echo "HOSTNAME: $HOSTNAME"
BK=101
if [[ $HOSTNAME =~ broker101 ]]; then
  BK=101
elif [[ $HOSTNAME =~ broker102 ]]; then
  BK=102
elif [[ $HOSTNAME =~ broker103 ]]; then
  BK=103
fi

# Configure Confluent Metrics Reporter for Control Center
echo "/etc/kafka/server.properties: metric.reporters=io.confluent.metrics.reporter.ConfluentMetricsReporter"
sudo sed -i "/metric.reporters=/c\metric.reporters=io.confluent.metrics.reporter.ConfluentMetricsReporter" /etc/kafka/server.properties
echo "/etc/kafka/server.properties: confluent.metrics.reporter.bootstrap.servers=localhost:9092"
sudo sed -i "/confluent.metrics.reporter.bootstrap.servers=/c\confluent.metrics.reporter.bootstrap.servers=localhost:9092" /etc/kafka/server.properties

# Set Broker id
echo "/etc/kafka/server.properties: broker.id=$BK"
echo "/etc/kafka/server.properties: broker.rack=r1"
sudo sed -i "/^broker.id/c\broker.id=$BK\nbroker.rack=r1" /etc/kafka/server.properties

# Set bootstrap.servers: Broker connection
sudo sed -i '/bootstrap.servers/c\bootstrap.servers=broker101:9092,broker102:9092,broker103:9092' /etc/kafka/connect-distributed.properties
sudo bash -c 'echo "producer.interceptor.classes=io.confluent.monitoring.clients.interceptor.MonitoringProducerInterceptor" >> /etc/kafka/connect-distributed.properties'
sudo bash -c 'echo "consumer.interceptor.classes=io.confluent.monitoring.clients.interceptor.MonitoringConsumerInterceptor" >> /etc/kafka/connect-distributed.properties'

# Configure server.properties: prevent timeouts
echo "/etc/kafka/server.properties: zookeeper.session.timeout.ms=30000"
sudo sed -i "/zookeeper.connection.timeout.ms=/c\zookeeper.session.timeout.ms=30000" /etc/kafka/server.properties

if [[ ! $HOSTNAME =~ confluent-training-vm ]]; then
  # Configure consumer offsets topic replication factor
  echo "/etc/kafka/server.properties: offsets.topic.replication.factor=3"
  sudo sed -i "/^offsets.topic.replication.factor/c\offsets.topic.replication.factor=3" /etc/kafka/server.properties
fi

# Configure server.properties: ZooKeeper connection
echo "/etc/kafka/server.properties: zookeeper.connect=zookeeper1:2181"
sudo sed -i '/zookeeper.connect=/c\zookeeper.connect=zookeeper1:2181' /etc/kafka/server.properties

if [[ $HOSTNAME =~ jumphost ]]; then
  # Configure kafka-rest.properties: Schema Registry connection
  sudo sed -i '/schema.registry.url=/c\schema.registry.url=http://schemaregistry1:8081' /etc/kafka-rest/kafka-rest.properties

  # Configure kafka-rest.properties: prevent timeouts
  sudo bash -c 'echo "consumer.request.max.bytes=102400" >> /etc/kafka-rest/kafka-rest.properties'

  # Configure kafka-rest.properties: ZooKeeper connection
  sudo sed -i '/zookeeper.connect=/c\zookeeper.connect=zookeeper1:2181' /etc/kafka-rest/kafka-rest.properties

  # Configure schema-registry.properties: ZooKeeper connection
  sudo sed -i '/kafkastore.connection.url=/c\kafkastore.connection.url=zookeeper1:2181' /etc/schema-registry/schema-registry.properties
fi

exit 0
