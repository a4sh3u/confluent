#!/usr/bin/env bash

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--create \
--topic performance \
--partitions 6 \
--replication-factor 3 )
echo "$RES"

RES=$( kafka-producer-perf-test \
--topic performance \
--num-records 10000000 \
--record-size 100 \
--throughput 10000000 \
--producer-props bootstrap.servers=broker101:9092,broker102:9092,broker103:9092 \
acks=1 )
echo "$RES"

RES=$( kafka-producer-perf-test \
--topic performance \
--num-records 10000000 \
--record-size 100 \
--throughput 10000000 \
--producer-props bootstrap.servers=broker101:9092,broker102:9092,broker103:9092 \
acks=all )
echo "$RES"

RES=$( kafka-producer-perf-test \
--topic performance \
--num-records 10000000 \
--record-size 100 \
--throughput 10000000 \
--producer-props bootstrap.servers=broker101:9092,broker102:9092,broker103:9092 \
acks=1 batch.size=1000000 linger.ms=1000 )
echo "$RES"

RES=$( kafka-producer-perf-test \
--topic performance \
--num-records 10000000 \
--record-size 100 \
--throughput 10000000 \
--producer-props bootstrap.servers=broker101:9092,broker102:9092,broker103:9092 \
acks=all batch.size=1000000 linger.ms=1000 )
echo "$RES"

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--create \
--topic i-love-logs \
--partitions 1 \
--replication-factor 1 )
echo "$RES"

RES=$( timeout 30s kafka-producer-perf-test \
--topic i-love-logs \
--num-records 10000000 \
--record-size 100 \
--throughput 1000 \
--producer-props bootstrap.servers=broker101:9092,broker102:9092,broker103:9092 )
echo "$RES"

RES=$( timeout 30s kafka-consumer-perf-test \
--broker-list broker101:9092 \
--topic i-love-logs \
--group test-group \
--messages 10000000 \
--threads 1 \
--show-detailed-stats \
--reporting-interval 5000 )
echo "$RES"

cd
printf 'fetch.min.bytes=100000\n' > consumer-perf.properties

RES=$( timeout 30s kafka-consumer-perf-test \
--broker-list broker101:9092 \
--topic i-love-logs \
--group test-group \
--messages 10000000 \
--threads 1 \
--show-detailed-stats \
--reporting-interval 5000 \
--consumer.config consumer-perf.properties )
echo "$RES"

echo "SUCCESS!"

exit 0
