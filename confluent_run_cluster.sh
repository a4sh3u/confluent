#!/usr/bin/env bash

if [[ ! $( ssh zookeeper1 exec "sudo service zookeeper status" ) =~ "ZooKeeper running" ]]; then
  ssh zookeeper1 exec "sudo /usr/local/bin/confluent_install_cp.sh"
  ssh zookeeper1 exec "sudo service zookeeper start"
  sleep 10
fi

WAITREQ=0
for BROKER in "broker101" "broker102" "broker103"
do
  if [[ ! $( ssh $BROKER exec "sudo service kafka-server status" ) =~ "Kafka Broker running" ]]; then
    ssh $BROKER exec "sudo /usr/local/bin/confluent_install_cp.sh"
    ssh $BROKER exec "sudo nohup service kafka-server start </dev/null &>/dev/null &"
    WAITREQ=1
  fi
done

if [ $WAITREQ == 1 ]; then
  echo "sleeping 20"
  sleep 20
fi

if [[ ! $( ssh schemaregistry1 exec "sudo service schema-registry status" ) =~ "Schema Registry running" ]]; then
  ssh schemaregistry1 exec "sudo service schema-registry start"
fi

if [[ ! $( ssh kafkarest1 exec "sudo service kafka-rest status" ) =~ "Kafka REST running" ]]; then
  ssh kafkarest1 exec "sudo service kafka-rest start"
fi

echo "Sleeping 5 seconds"
sleep 5

if [[ ! $( zookeeper-shell zookeeper1:2181 ls /brokers/ids ) =~ "101, 102, 103" ]]; then
  echo "ERROR: Brokers ->  We're sorry but something went wrong. Please work with your instructor to get your environment working"
  exit 1
fi

if [[ ! $( ssh schemaregistry1 exec "sudo jps | grep SchemaRegistry" ) =~ "SchemaRegistry" ]]; then
  echo "ERROR: Schema Registry -> We're sorry but something went wrong. Please work with your instructor to get your environment working"
  exit 1
fi

if [[ ! $( ssh kafkarest1 exec "sudo jps | grep KafkaRest" ) =~ "KafkaRest" ]]; then
  echo "ERROR: REST Proxy -> We're sorry but something went wrong. Please work with your instructor to get your environment working"
  exit 1
fi

/usr/local/bin/confluent_control_center_hard_reset.sh

echo -e "\nSUCCESS!"

exit 0
