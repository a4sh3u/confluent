#!/usr/bin/env bash

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--create \
--topic replicated-topic \
--partitions 6 \
--replication-factor 2 )
echo "$RES"

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--describe \
--topic replicated-topic )
echo "$RES"

RES=$( kafka-producer-perf-test \
--topic replicated-topic \
--num-records 6000 \
--record-size 100 \
--throughput 1000 \
--producer-props bootstrap.servers=broker101:9092,broker102:9092,broker103:9092 )
echo "$RES"

echo "I heart logs" | kafka-console-producer --broker-list broker101:9092 --topic replicated-topic
echo "Hello world" | kafka-console-producer --broker-list broker101:9092 --topic replicated-topic
echo "All your base" | kafka-console-producer --broker-list broker101:9092 --topic replicated-topic
echo "Kafka rules" | kafka-console-producer --broker-list broker101:9092 --topic replicated-topic
echo "Don't worry" | kafka-console-producer --broker-list broker101:9092 --topic replicated-topic
echo "Be happy" | kafka-console-producer --broker-list broker101:9092 --topic replicated-topic

for BROKER in "broker101" "broker102" "broker103"
do
  echo "$BROKER:"
  ssh $BROKER exec "ls /var/lib/kafka/replicated-topic* | grep rep"
  ssh $BROKER exec "grep replicated-topic /var/lib/kafka/*checkpoint"
done

echo "SUCCESS!"

exit 0
