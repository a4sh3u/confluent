#!/usr/bin/env bash

BACKUPPATH="/home/training/backup"

for BROKER in "broker101" "broker102" "broker103"
do
  if [ ! -f "/home/training/ssl/certs/kafka.$BROKER.truststore.jks" ] && [ ! -f "/home/training/ssl/keys/kafka.$BROKER.keystore.jks" ]; then
    continue
  fi
  if [ $( ssh training@$BROKER exec "tree ssl" | grep jks | wc -l ) != 2 ]; then
    scp /home/training/ssl/certs/kafka.$BROKER.truststore.jks training@$BROKER:/home/training/ssl/certs/.
    scp /home/training/ssl/keys/kafka.$BROKER.keystore.jks training@$BROKER:/home/training/ssl/keys/.
    ssh $BROKER exec "chown training:training /home/training/ssl -R"
    if [ $( ssh training@$BROKER exec "tree ssl" | grep jks | wc -l ) == 2 ]; then
      mv /home/training/ssl/certs/kafka.$BROKER.truststore.jks $BACKUPPATH/.
      mv /home/training/ssl/keys/kafka.$BROKER.keystore.jks $BACKUPPATH/.
    fi
  else
    mv /home/training/ssl/certs/kafka.$BROKER.truststore.jks $BACKUPPATH/.
    mv /home/training/ssl/keys/kafka.$BROKER.keystore.jks $BACKUPPATH/.
  fi
done

exit 0
