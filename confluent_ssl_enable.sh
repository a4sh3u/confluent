#!/usr/bin/env bash

for BROKER in "broker101" "broker102" "broker103"
do
  ssh $BROKER exec "sudo nohup service kafka-server stop </dev/null &>/dev/null &"
  ssh $BROKER exec "sudo bash -c 'cat /home/training/ssl/ssl.properties >> /etc/kafka/server.properties'"
  ssh $BROKER exec "sudo bash -c 'echo "ssl.client.auth=required" >> /etc/kafka/server.properties'"
done

echo "Sleeping 30 seconds...waiting for the Brokers to stop"
sleep 30

for BROKER in "broker101" "broker102" "broker103"
do
  ssh $BROKER exec "sudo nohup service kafka-server start </dev/null &>/dev/null &"
done

echo "Sleeping 30 seconds...waiting for the Brokers to start"
sleep 30

ssh zookeeper1 exec "echo 'ssl.keystore.location=/home/training/ssl/keys/kafka.jumphost.keystore.jks' >> client.properties"
ssh zookeeper1 exec "echo 'ssl.keystore.password=training' >> client.properties"

if [[ ! $( zookeeper-shell zookeeper1:2181 ls /brokers/ids ) =~ "101, 102, 103" ]]; then
  echo "ERROR: Brokers ->  We're sorry but something went wrong. Please work with your instructor to get your environment working"
  exit 1
fi

for BROKER in "broker101" "broker102" "broker103"
do
  if [[ $( openssl s_client -connect $BROKER:9093 -tls1 2>&1 ) =~ "Connection refused" ]]; then
    echo "ERROR: openssl ->  We're sorry but something went wrong. Please work with your instructor to get your environment working"
    exit 1
  fi
done

echo "SUCCESS!"

exit 0

