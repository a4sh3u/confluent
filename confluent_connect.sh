#!/usr/bin/env bash

sudo connect-distributed -daemon /etc/kafka/connect-distributed.properties

echo "Waiting for connect to start.  Sleeping 60 seconds"
sleep 60

echo "SELECT * FROM years;" | sqlite3 /usr/local/lib/my.db

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--create \
--topic shakespeare_years \
--partitions 1 \
--replication-factor 1 \
--if-not-exists )
echo "$RES"

sleep 20

curl -X POST -H "Content-Type: application/json" \
--data @/usr/local/etc/connector-source-jdbc-properties.json http://connect1:8083/connectors

sleep 5

curl -X POST -H "Content-Type: application/json" \
--data @/usr/local/etc/connector-sink-file-properties.json http://connect1:8083/connectors

echo "Waiting for connect pipeline.  Sleeping 30 seconds"
sleep 30

if [ ! -f "test.sink.txt" ]; then
  echo "ERROR: Something went wrong. The file test.sink.txt does not exist"
  exit 1
fi

cat test.sink.txt

echo "SUCCESS!"

exit 0
