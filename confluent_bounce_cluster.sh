#!/usr/bin/env bash

if [[ ! $( ssh zookeeper1 exec "sudo service zookeeper status" ) =~ "ZooKeeper running" ]]; then
  exit 0
else
  sleep 10
  /usr/local/bin/confluent_run_cluster.sh
fi

exit 0
