#!/usr/bin/env bash

if [[ $( ssh jumphost exec "yum -q list installed confluent-control-center 2>&1" ) =~ "Error: No matching Packages to list" ]]; then
  echo "ERROR: Confluent Control Center is not installed yet. First complete the Kafka Installation exercise or run confluent_run_cluster.sh"
  exit 1
fi

echo -e "Resetting Control Center will take about 3 minutes..."

# Hard kill existing Control Center process
ssh jumphost exec "sudo jps | grep ControlCenter | awk '{print $1;}' | xargs sudo kill -9"

sleep 80

# Reset Control Center variables, delete internal topics
ssh jumphost exec "sudo control-center-reset /home/training/control-center.properties </dev/null"
ssh jumphost exec "kafka-topics --zookeeper zookeeper1:2181 --delete --if-exists --topic _confluent-command </dev/null"
ssh jumphost exec "kafka-topics --zookeeper zookeeper1:2181 --delete --if-exists --topic _confluent-monitoring </dev/null"

ssh jumphost exec "sudo rm -fr /tmp/confluent"
ssh jumphost exec "sudo rm -fr /tmp/control-center-logs"
ssh jumphost exec "sudo rm -fr /tmp/lib*.so"
ssh jumphost exec "sudo rm -fr /var/lib/confluent-control-center"

sleep 10

# Restart Control Center
ssh jumphost exec "nohup sudo control-center-start /home/training/control-center.properties </dev/null &>/dev/null &"

echo -e "Sleeping 90 seconds..."
sleep 90

OUTPUT=$(curl -X get http://jumphost:9021/2.0/clusters/kafka/)
IDPROD=$(echo $OUTPUT | jq --raw-output ".[0].clusterId")
curl -X PATCH  -H "Content-Type: application/merge-patch+json" -d '{"displayName":"Confluent Training"}' http://jumphost:9021/2.0/clusters/kafka/$IDPROD

curl -X POST -H "Content-Type: application/json" -d '{"name":"Consumption Difference cg","clusterId":"'$(curl -X get http://jumphost:9021/2.0/clusters/kafka/ | jq --raw-output .[0].clusterId)'","group":"cg","metric":"CONSUMPTION_DIFF","condition":"GREATER_THAN","longValue":"0","lagMs":"5000"}' http://jumphost:9021/2.0/alerts/triggers
curl -X POST -H "Content-Type: application/json" -d '{"name":"Consumption Difference cg-fetch-min","clusterId":"'$(curl -X get http://jumphost:9021/2.0/clusters/kafka/ | jq --raw-output .[0].clusterId)'","group":"cg-fetch-min","metric":"CONSUMPTION_DIFF","condition":"GREATER_THAN","longValue":"0","lagMs":"5000"}' http://jumphost:9021/2.0/alerts/triggers
curl -X POST -H "Content-Type: application/json" -d '{"name":"Under Replicated Partitions","clusterId":"default","condition":"GREATER_THAN","longValue":"0","lagMs":"60000","brokerClusters":{"brokerClusters":["'$(curl -X get http://jumphost:9021/2.0/clusters/kafka/ | jq --raw-output ".[0].clusterId")'"]},"brokerMetric":"UNDER_REPLICATED_TOPIC_PARTITIONS"}' http://jumphost:9021/2.0/alerts/triggers
curl -X POST -H "Content-Type: application/json" -d '{"name":"Email Administrator","enabled":true,"triggerGuid":["'$(curl -X get http://jumphost:9021/2.0/alerts/triggers/ | jq --raw-output .[0].guid)'","'$(curl -X get http://jumphost:9021/2.0/alerts/triggers/ | jq --raw-output .[1].guid)'","'$(curl -X get http://jumphost:9021/2.0/alerts/triggers/ | jq --raw-output .[2].guid)'"],"maxSendRateNumerator":1,"intervalMs":"60000","email":{"address":"devnull@confluent.io","subject":"Confluent Control Center alert"}}' http://jumphost:9021/2.0/alerts/actions

exit 0

