#!/usr/bin/env bash

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--create \
--topic grow-topic \
--partitions 6 \
--replication-factor 3 )
echo "$RES"

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--describe \
--topic grow-topic )
echo "$RES"

echo "a" | kafka-console-producer --broker-list broker101:9092 --topic grow-topic
echo "b" | kafka-console-producer --broker-list broker101:9092 --topic grow-topic
echo "c" | kafka-console-producer --broker-list broker101:9092 --topic grow-topic

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--alter \
--topic grow-topic \
--partitions 12 )
echo "$RES"

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--describe \
--topic grow-topic )
echo "$RES"

echo "I" | kafka-console-producer --broker-list broker101:9092 --topic new-topic
echo "Love" | kafka-console-producer --broker-list broker101:9092 --topic new-topic
echo "Kafka" | kafka-console-producer --broker-list broker101:9092 --topic new-topic

RES=$( kafka-topics \
--zookeeper zookeeper1:2181 \
--describe \
--topic new-topic )
echo "$RES"

RES=$( timeout 30s kafka-producer-perf-test --topic grow-topic \
--num-records 1000000 --record-size 100 --throughput 1000 \
--producer-props bootstrap.servers=broker101:9092,broker102:9092,broker103:9092 )
echo "$RES"

echo "SUCCESS!"

exit 0
